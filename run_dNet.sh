CONS_DOMAIN=$1
ORG_NAME=$2
PORT=$3
ADMIN_MODE=$4
DEBUG_MODE=$5

ORG_LOWER=$(echo "$ORG_NAME" | awk '{print tolower($0)}')
ORG_DOMAIN=${ORG_LOWER}.${CONS_DOMAIN}
CLI_DOMAIN=cli.${ORG_LOWER}

MATERIAL_DIR="$PWD/$ORG_NAME"
mkdir -p $MATERIAL_DIR

docker run -d --name $CLI_DOMAIN -p $PORT:80 \
-e HOST_PWD="$MATERIAL_DIR" \
-e ADMIN_MODE="$ADMIN_MODE" \
-e DEBUG_MODE="$DEBUG_MODE" \
-v "/var/run/docker.sock":"/var/run/docker.sock" \
-v "$MATERIAL_DIR/crypto-config":"/home/node/app/crypto-config" \
-v "$MATERIAL_DIR/channel-artifacts":"/home/node/app/channel-artifacts" \
-v "$MATERIAL_DIR/wallet":"/home/node/app/wallet" \
-v "$MATERIAL_DIR/config":"/home/node/app/config" \
-v "$MATERIAL_DIR/explorer":"/home/node/app/explorer" \
-v "$MATERIAL_DIR/log":"/home/node/app/log" \
-v "$MATERIAL_DIR/plugins":"/plugins" \
dcentra/dnet $CONS_DOMAIN $ORG_NAME $DEBUG_MODE
