const path = require('path');
const { to } = require('await-to-js');
const express = require('express');
const bodyParser = require('body-parser');

const query = require('./sdk/query');
const invoke = require('./sdk/invoke');

const ccName = 'dMsg';

const app = express();
app.use(bodyParser.raw({type: 'application/octet-stream', limit : '2mb'}));
app.use(bodyParser.json());
app.use('/dapp_dMsg', express.static('www'));
app.use(express.static('www'));

app.get('/api/chaincodeContext', async function(req, res) {
  res.json({
     name: ccName,
     version: '1.0', // TODO read from package.json
     lang: 'node',
     init: '[]'
  });
});

app.get('/api/chaincode', async function(req, res) {
    res.sendFile('/home/node/app/chaincode.tar.gz');
});

app.get('/dapp_dMsg/api/messages', async function(req, res) {
  const [error, result] = await to(query(ccName, 'getMessages'));
  if(error) {
    return res.status(500).send(error);
  }

  res.json(result);
});

app.post('/dapp_dMsg/api/publishMessage', async function(req, res) {
  const body = req.body;
  const [error] = await to(invoke(ccName, 'publishMessage', body.message, process.env.CLI_DOMAIN));
  if(error) {
    return res.status(500).send(error);
  }

  res.send();
});
// frontend fallback route
app.get('*', (req, res) => res.sendFile(path.join(__dirname, 'www', 'index.html')));

const PORT = process.env.PORT || 80;
app.listen(PORT, function() {
  console.log(`Backend listening on port ${PORT}!`);
});
