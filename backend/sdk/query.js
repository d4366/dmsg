'use strict';

const rp = require('request-promise');

module.exports = async function(chaincode, method, ...args) {
    return rp({
        uri: `http://${process.env.CLI_DOMAIN}/api/query`,
        method: 'POST',
        headers: {Authorization: process.env.AUTH_STRING},
        body: {chaincode, method, args},
        json: true
    }).then(r => JSON.parse(Buffer.from(r.result.data).toString()));
}
