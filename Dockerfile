FROM node:8 AS ccbuilder

WORKDIR /home/node/
COPY chaincode/ chaincode/
RUN cd chaincode && yarn && yarn build && rm -rf node_modules
RUN tar -zcvf chaincode.tar.gz -C chaincode .

FROM node:10 AS builder

WORKDIR /home/node/app/
COPY frontend/ ./
RUN npm i && npm run build -- --prod --base-href /dapp_dMsg/

FROM node:10

WORKDIR /home/node/app/

COPY backend ./
COPY --from=ccbuilder /home/node/chaincode.tar.gz chaincode.tar.gz
COPY --from=builder /home/node/app/www/ www/
RUN npm install --production

EXPOSE 80
CMD ["npm", "start"]
