import { differenceWith, isEqual } from 'lodash';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { interval, Observable, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
  public folder: string;
  public messages: any[] = [];
  private pollMessages: Subscription;

  constructor(
    public http: HttpClient,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.folder = this.activatedRoute.snapshot.paramMap.get('id');

    this.pollMessages = interval(1000).
      pipe(switchMap(this.getMessages.bind(this))).
      subscribe(messages =>
        // append difference to existing messages
        Array.prototype.push.apply(this.messages, differenceWith(messages, this.messages, isEqual))
      );
  }

  getMessages(): Observable<any[]> {
    return this.http.get<any[]>('/dapp_dMsg/api/messages');
  }

  async sendMessage(event) {
    await this.http.post('/dapp_dMsg/api/publishMessage', event).toPromise();
  }

}
