import { Chaincode, StubHelper } from '@theledger/fabric-chaincode-utils';

import { LedgerArray } from './ledgerArray';

export class MessageStore extends Chaincode {

    /* tslint:disable:no-empty */
    async init() {}

    /* tslint:disable:no-empty */
    async heartbeat() {
        // makes sure we can reach the chaincode from outside
    }

    async getMessages(stubHelper: StubHelper): Promise<any[]> {
        const messages = LedgerArray.instance(stubHelper, 'messages');
        return messages.getAll().map(s => JSON.parse(s));
    }

    async publishMessage(stubHelper: StubHelper, [message, sender]: [string, string]) {
        const messages = LedgerArray.instance(stubHelper, 'messages');
        await messages.push(JSON.stringify({message, sender}));
    }
};
