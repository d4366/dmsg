import * as Bluebird from 'bluebird';
import { StubHelper } from '@theledger/fabric-chaincode-utils';

export class LedgerTreeMap {
    static instance(stubHelper: StubHelper, objectType: string) {
        return new LedgerTreeMap(stubHelper, objectType);
    }
    
    constructor(public stubHelper: StubHelper, public objectType: string) {
    }

    async get(subkeys: string[]) : Promise<string> {
        const compositeKey = await this.stubHelper.getStub().createCompositeKey(this.objectType, subkeys);
        
        return this.stubHelper.getStateAsString(compositeKey);
    }
    async getHistory(subkeys: string[]) : Promise<object[]> {
        const compositeKey = await this.stubHelper.getStub().createCompositeKey(this.objectType, subkeys);
        
        return this.stubHelper.getHistoryForKeyAsList(compositeKey);
    }
    async getKeys(subkeys: string[], bookmark?: string, pageSize?: number) : Bluebird<string[]> {
        const iterator: any = await this.stubHelper.getStub().getStateByPartialCompositeKey(this.objectType, subkeys);

        const results = [];
        while(true) {
            const responseRange = await iterator.next();
            if (!responseRange || !responseRange.value || !responseRange.value.key) {
                break;
            }

            const {attributes} = await this.stubHelper.getStub().splitCompositeKey(responseRange.value.key);
            results.push(this.objectType + attributes.join(''));
        }
        
        return results;
    }
    async getValues(subkeys: string[], bookmark?: string, pageSize?: number) : Bluebird<string[]> {
        const iterator: any = await this.stubHelper.getStub().getStateByPartialCompositeKey(this.objectType, subkeys);

        const results = [];
        while(true) {
            const responseRange = await iterator.next();
            if (!responseRange || !responseRange.value || !responseRange.value.key) {
                break;
            }

            results.push(responseRange.value.value.toUTF8 ? responseRange.value.value.toUTF8() : responseRange.value.value.toString());
        }
        
        return results;
    }
    
    async getSome(subkeys: string[], bookmark?: string, pageSize?: number) : Bluebird<any> {
        const iterator: any = await this.stubHelper.getStub().getStateByPartialCompositeKey(this.objectType, subkeys);

        const result = [];
        while(true) {
            const responseRange = await iterator.next();
            if (!responseRange || !responseRange.value || !responseRange.value.key) {
                break;
            }

            const {attributes} = await this.stubHelper.getStub().splitCompositeKey(responseRange.value.key);

            result.push([attributes, responseRange.value.value.toUTF8 ? responseRange.value.value.toUTF8() : responseRange.value.value.toString()]);
        }

        return result;
    }
    async getSomeAsObjects(subkeys: string[], bookmark?: string, pageSize?: number) : Bluebird<any> {
        const iterator: any = await this.stubHelper.getStub().getStateByPartialCompositeKey(this.objectType, subkeys);

        const result = {};
        while(true) {
            const responseRange = await iterator.next();
            if (!responseRange || !responseRange.value || !responseRange.value.key) {
                break;
            }

            const {attributes} = await this.stubHelper.getStub().splitCompositeKey(responseRange.value.key);

            let last;
            let traverser = result;
            for(const a of attributes) {
                last = traverser;
                traverser = traverser[a] ? traverser[a] : (traverser[a] = {});
            }
            //traverser = responseRange.value.value.toString();
            last[attributes[attributes.length-1]] = responseRange.value.value.toUTF8 ?
                responseRange.value.value.toUTF8() : responseRange.value.value.toString();
        }

        return result;
    }
    async getSomeAsFlatObjects(subkeys: string[], bookmark?: string, pageSize?: number) : Bluebird<any> {
        const iterator: any = await this.stubHelper.getStub().getStateByPartialCompositeKey(this.objectType, subkeys);

        const result = {};
        while(true) {
            const responseRange = await iterator.next();
            if (!responseRange || !responseRange.value || !responseRange.value.key) {
                break;
            }

            const {attributes} = await this.stubHelper.getStub().splitCompositeKey(responseRange.value.key);

            result[attributes.join('_')] = responseRange.value.value.toUTF8 ?
                responseRange.value.value.toUTF8() : responseRange.value.value.toString();
        }

        return result;
    }

    async put(subkeys: string[], val: string, overwrite?: boolean) {
        const compositeKey = await this.stubHelper.getStub().createCompositeKey(this.objectType, subkeys);
        
        await this.stubHelper.putState(compositeKey, val);
    }

    async remove(subkeys: string[]) {
        const compositeKey = await this.stubHelper.getStub().createCompositeKey(this.objectType, subkeys);
        
        await this.stubHelper.getStub().deleteState(compositeKey);
    }
}
