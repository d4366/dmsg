import {isUndefined} from 'lodash';
import range from 'iterable-range';
import * as Bluebird from 'bluebird';
import { StubHelper } from '@theledger/fabric-chaincode-utils';
import { LedgerTreeMap } from './ledgerTreeMap';

/*
 reserved always corresponds to the number of total ever used elements
 size corresponds to reserved minus the deleted elements.
 */
export class LedgerArray {
    map: LedgerTreeMap;

    static instance(stubHelper: StubHelper, name: string) {
        return new LedgerArray(stubHelper, name);
    }
    
    constructor(public stubHelper: StubHelper, public name: string) {
        this.map = new LedgerTreeMap(stubHelper, name);
    }
    
    async size() : Promise<number> {
        return Number(await this.stubHelper.getStateAsObject(this.name + '_size'));
    }
    async reserved() : Promise<number> {
        return Number(await this.stubHelper.getStateAsObject(this.name + '_resrv'));
    }
    async clear() {
        // FIXME needs to be atomic?
        await this.resize(0);
        await this.reserve(0);
    }

    async get(index: number) : Promise<string> {
        return this.stubHelper.getStateAsString(this.name + '_' + index);
    }
    async getHistory(index: number) : Promise<object[]> {
        return this.stubHelper.getHistoryForKeyAsList(this.name + '_' + index);
    }
    async getSome(first?: number, offset = 0) : Bluebird<string[]> {
        if(isUndefined(first)) {
            first = await this.reserved();
        }

        return this.map.getValues([]);
    }
    async getAll() : Bluebird<string[]> {
        return this.getSome();
    }
    /*async getSomeHistory(first?: number, offset = 0) : Bluebird<object[][]> {
        if(isUndefined(first)) {
            first = await this.reserved();
        }

        return Bluebird.filter(
            range(offset, offset+first), (i: number) => this.isRemoved(i).then(r => !r)
        ).map((i: number) => this.getHistory(i));
    }
    async getAllHistory() : Bluebird<object[][]> {
        return this.getSomeHistory();
    }*/
    async lastIndex() : Promise<number> {
        const reserved = await this.reserved();

        // TODO Bluebird.race
        for(let i=reserved-1; i>=0; --i) {
            const isRemoved = await this.isRemoved(i);
            if(!isRemoved) {
                return i;
            }
        }

        return -1;
    }

    async put(index: number, value: any) {
        //await this.stubHelper.putState(this.name + '_' + index, value);
        await this.map.put([index.toString()], value);
        
        if(await this.isRemoved(index)) {
            await this.setRemoved(index, false);
        }
    }

    /*
    WARNING Do NOT call multiple times in one transaction. Use pushArray instead.
    
    see https://hyperledger-fabric.readthedocs.io/en/release-1.3/readwrite.html
    Further, if the transaction writes a value multiple times for a key,
    only the last written value is retained.
    Also, if a transaction reads a value for a key,
    the value in the committed state is returned even if the transaction
    has updated the value for the key before issuing the read.
    In another words, Read-your-writes semantics are not supported.    */
    async push(value: any) {
        const lastIndex = await this.lastIndex();
        const newIndex = lastIndex+1;

        await this.put(newIndex, value);
        await this.resize((await this.size())+1);

        const reserved = await this.reserved();
        if(newIndex+1 >= reserved) {
            await this.reserve(newIndex+1);
        }
    }
    async pushArray(values: any[]) {
        const numValues = values.length;
        const baseIndex = await this.reserved();

        const putPromises = Bluebird.map(
            range(0, numValues), (i: number) => this.put(baseIndex+i, values[i])
        );
        
        await Bluebird.all([
            putPromises,
            this.size().then((size: number) => this.resize(size+numValues)),
            this.reserve(baseIndex+numValues)
        ]);
    }
    async pop() : Promise<any> {
        const lastIndex = await this.lastIndex();
        
        const popped = await this.get(lastIndex);
        await this.remove(lastIndex);
        
        return popped;
    }
    
    async find(value: any) : Promise<number> {
        const values = await this.getAll();
        
        let index = 0;
        for(let val of values) {
            if(val == value) {
                return index;
            }
            
            ++index;
        }
        
        return -1;
    }

    async isRemoved(index: number) : Promise<boolean> {
        return (await this.stubHelper.getStateAsObject(this.name + '_' + index + '_dele')) as boolean;
    }
    async remove(index: number) {
        await this.resize((await this.size())-1);

        const reserved = await this.reserved();
        if(index == reserved-1) {
            await this.reserve(reserved-1);
        } else {
            await this.setRemoved(index, true);
        }
    }

    private async resize(size: number) {
        /*const reserved = await this.reserved();
        if(size > reserved) {
            await this.reserve(size);
        }*/
        
        await this.stubHelper.putState(this.name + '_size', size);
    }
    private async reserve(size: number) {
        await this.stubHelper.putState(this.name + '_resrv', size);
    }
    private async setRemoved(index: number, removed: boolean) {
        await this.stubHelper.putState(this.name + '_' + index + '_dele', removed);
    }
}
