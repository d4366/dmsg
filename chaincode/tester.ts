/* tslint:disable */

import * as express from 'express';
import * as bodyParser from 'body-parser';

import { MessageStore } from './src/messageStore';
import { ChaincodeMockStub, Transform } from '@theledger/fabric-mock-stub';

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.text());

let chaincode, stub;
async function init() {
    chaincode = new MessageStore();
    stub = new ChaincodeMockStub("MyMockStub", chaincode);
    await stub.mockInit("tx1", []);
}
const initPromise = init();

async function execQueryOrInvoke(req, res) {
    await initPromise;
    
    const result = await stub.mockInvoke("txID", [req.body.method, ...req.body.args]);
    res.json({result: result.payload});
};

app.post('/api/query', execQueryOrInvoke);
app.post('/api/invoke', execQueryOrInvoke);

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => console.log(`Tester listening on port ${PORT}!`));