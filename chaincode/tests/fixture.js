const chance = require('chance');
const moment = require('moment');

/*
Asset (Personal Jesus, ISWC: T-909.264.922-4)

    Asset Identifier

    Asset Type (only Music Right for the moment)

    Asset Description

    Asset Creation Date

    Asset Creators (Identities)

    Asset Name

    Asset URL

    Proof-of-Asset (Document)

    Asset Registration Authority

    Asset Registration Office Address

    …
*/
exports.MOCK_ASSETS = {
    '18 (Clifford/Daunt/Hemmings/Howes/Stannard/Stride)': {
        type: 'MusicRight',
        description: 'Lalala',
        creationDate: '2019-01-01',
        creators: [ 'Richard' ],
        name: '18',
        url: 'http://',
        proof: 'ipfs/',
        regAuthority: 'ISWC',
        regAuthorityOfficeAddress: '',
        authors: [ 'Clifford',' Daunt', 'Hemmings', 'Howes', 'Stannard', 'Stride' ]
    }
};
exports.newAsset = () => {
    return [chance.cpf(), {
        type: 'MusicRight',
        description: chance.sentence(),
        creationDate: moment(chance.date()).toISOString(),
        creators: [ 'Richard' ],
        name: chance.first()+'_'+chance.animal(),
        url: chance.url(),
        proof: 'ipfs/',
        regAuthority: chance.company(),
        regAuthorityOfficeAddress: chance.address(),
        authors: [ 'Clifford',' Daunt', 'Hemmings', 'Howes', 'Stannard', 'Stride' ]
    }];
};

/*
    Identity (Andrew Goodwin)
    
    Identity Number
    
    Proof-of-Identity (Document)
    
    Entity Type (Natural Person, Organization, …)
    
    Identity Number Type
    
    Identity Number Expiration Date (Validity Period)
    
    Identity Number Verification Authority
    
    Identity Number Verification Office Address
    
    User Name
    
    Contact Email
    
    Contact Address
    
    DID Identifier (Optional)
    
    …
*/

exports.MOCK_IDENTITIES = {
    'Andrew Goodwin': {
        proof: 'driver license',
        entityType: 'driver license',
        identityNumberType: 'driver license',
        identityNumberExpiration: '01.01.2025',
        identityNumberVerificationAuthority: 'Department of Motor Vehicles',
        identityNumberVerificationOfficeAddress: 'driver license',
        userName: 'andrew',
        contactEmail: 'andrew.goodwin@test.com',
        contactAddress: 'Switzerland',
        DIDidentifier: 'DID'
    }
};
exports.newIdentity = () => {
    return [chance.cpf(), {
        proof: 'driver license',
        entityType: 'driver license',
        identityNumberType: 'driver license',
        identityNumberExpiration: '01.01.2025',
        identityNumberVerificationAuthority: 'Department of Motor Vehicles',
        identityNumberVerificationOfficeAddress: 'driver license',
        userName: 'andrew',
        contactEmail: chance.email(),
        contactAddress: chance.country(),
        DIDidentifier: 'DID'
    }];
};

/*
Asset Right (Streaming Perf.)

    Asset Right Type (depends on the Asset Type, for Music Right it is Download Right, Performance Right, …)

    Asset Right Description

    Asset Right Regulatory Authority

    Asset Right Regulatory Office Address

    Asset Right URL

    …
*/
exports.MOCK_ASSET_RIGHTS = {
    'Streaming Perf.': {
        type: 'Performance',
        description: 'Lalala',
        regAuthority: 'ISWC',
        regAuthorityOfficeAddress: '',
        url: ''
    }
};
exports.newAssetRight = () => {
    return ['Streaming Perf.', {
        type: 'Performance',
        description: chance.sentence(),
        regAuthority: chance.company(),
        regAuthorityOfficeAddress: chance.address(),
        url: chance.url()
    }];
};

/*
Asset Right Assignment (Andrew Goodwin - ISWC: T-909.264.922-4 : Streaming Perf.)

    Asset Right Assignment Identifier

    Asset Identifier

    Asset Right Type

    Identity Number

    Share (Percentage of Asset Right a certain Identity owns in a particular Asset Right Assignment Context)

    Assignment Description

    Assignment Authority

    Assignment Authority Office Address

    Proof-of-Assignment (Document)

    …
*/
exports.MOCK_ASSET_RIGHT_ASSIGNMENTS = {
    '18 (Clifford/Daunt/Hemmings/Howes/Stannard/Stride)_Andrew Goodwin_Streaming Perf.': {
        "identityNumber": "", // ???
        
        "share": 60.00,
        
        "description": "?",
        
        "authority": "London Lawyer?",
        "authorityAddress": "",
        
        "proof": "/ipfs"
    }
};
exports.newAssetRightAssignment = (owner, asset, right) => {
    return [asset+'_'+owner+'_'+right, {
        "identityNumber": "", // ???
        
        "share": chance.floating(),
        
        "description": chance.sentence(),
        
        "authority": chance.company(),
        "authorityAddress": chance.address(),
        
        "proof": "/ipfs"
    }];
};

/*
Asset Right Assignment Context (Andrew Goodwin - ISWC: T-909.264.922-4 : Streaming Perf. - Context)

    Asset Right Assignment Identifier

    Assignment Start Date Time

    Assignment End Date Time

    Assignment Location (Global, list of Regions / Countries)

    Assignment Conditions (Free Text for now)

    Max Royalty Collected

    Max Number of Uses Permitted

    Allowed Mediums (limit right to particular set of Mediums)

    …
*/
exports.MOCK_ASSET_RIGHT_ASSIGNMENT_CONTEXTS = {
    '18 (Clifford/Daunt/Hemmings/Howes/Stannard/Stride)_Andrew Goodwin_Streaming Perf._Context': {
        "period":{
            "startDate":"2018-01-01",
            "endDate":"2018-12-31"
        },
        "location": "Bel",
        "conditions": "",
        
        "maxRoyaltyCollected": "?", // required?
        "maxNumberOfUsesPermitted": -1, // -1 for unlimited
        "allowedMediums": "CD"
    }
};
exports.newAssetRightAssignmentContext = (owner, asset, right) => {
    return [asset+'_'+owner+'_'+right+'_Context', {
        "period": newPeriod(),
        "location": chance.country(),
        "conditions": chance.paragraph(),
        
        "maxRoyaltyCollected": "?", // required?
        "maxNumberOfUsesPermitted": -1, // -1 for unlimited
        "allowedMediums": "CD"
    }];
};

/*
Royalty Payment (BMG Q1Y19)

    Royalty Payment Identifier

    Royalty Payment Submitter (Identity Number)

    Royalty Payment Submitting Date Time

    Proof-of-Royalty (Document)

    …
*/
exports.MOCK_ROYALTY_PAYMENTS = {
    "BMG Q1Y19": {
        "submitter": "Richard",
        "timeSubmitted": "2019-01-01",
        "proof": "/ipfs"
    },
};
exports.newRoyaltyPayment = () => {
    return ["BMG Q1Y19", {
        "submitter": "Richard",
        "timeSubmitted": moment(chance.date()).toISOString(),
        "proof": "/ipfs"
    }];
};

/*
Royalty Payment Instruction (BMG Q1Y19 - Line 66)

    Royalty Payment Instruction Identifier

    Royalty Payment Identifier

    Asset Identifier

    Asset Right Type

    Royalty Payment Amount

    Royalty Payment Currency

    Royalty Payment Party (Identity Number)

    …
    
    Composite Key RoyaltyPaymentID_LineNumber_PartNumber
*/
exports.MOCK_ROYALTY_PAYMENT_INSTRUCTIONS = {
    "18 (Clifford/Daunt/Hemmings/Howes/Stannard/Stride)_Andrew Goodwin_BMG Q1Y19": {
        "assetId": "18 (Clifford/Daunt/Hemmings/Howes/Stannard/Stride)",
        "rightType": "Streaming Perf.",
        
        "currency": "GBP",
        "amount": "0.01",
        
        "period":{
            "startDate":"2018-01-01",
            "endDate":"2018-12-31"
        },

        // required?
        "catalogueNo":"SPOTIFY AB",
        "country":"Bel",
        "sourceName":"Aresa Belgium (Pan�)",
        "units":"412",
    },
};
exports.newRoyaltyPaymentInstruction = (asset, owner, payment) => {
    return [asset+"_"+owner+"_"+payment, {
        "assetId": asset,
        "rightType": "Streaming Perf.",
        
        "currency": chance.currency(),
        "amount": chance.floating(),
        
        "period": newPeriod(),

        // required?
        "catalogueNo": "SPOTIFY AB",
        "country": chance.country(),
        "sourceName": "Aresa Belgium (Pan�)",
        "units": chance.natural({max: 1000}),
    }];
};

/*
Royalty Payment Context (BMG Q1Y19 - Line 66 - Context)

    Royalty Payment Instruction Identifier

    Event Identifier

    Event Date Time

    Event Start Date Time

    Event End Date Time

    Event Location

    Event Source

    Event Channel

    Event Quantity

    Event URL

    Additional Event Metrics (Free Text for now)

    Asset Identifier

    Asset Right Utilized

    …
*/
exports.MOCK_ROYALTY_PAYMENT_CONTEXTS = { // context = event?
    "18 (Clifford/Daunt/Hemmings/Howes/Stannard/Stride)_Andrew Goodwin_BMG Q1Y19_Context": {
        "royaltyPaymentInstruction": "BMG 18 (Clifford/Daunt/Hemmings/Howes/Stannard/Stride)_Andrew Goodwin_BMG Q1Y19",

        "eventPeriod":{
            "startDate":"01-01-2018",
            "endDate":"01-01-2018"
        },
        "location": "Bel",
        "source": "Aresa Belgium (Pan�)",
        "channel": "SPOTIFY AB",
        "quantity": "412",
        "url": "",
        "extraMetrics": "",
        
        //"assetId": "18 (Clifford/Daunt/Hemmings/Howes/Stannard/Stride)",
        //"assetRight": "Streaming Perf."
    },
};

exports.newRoyaltyPaymentContext = (asset, owner, payment) => { // context = event?
    return [asset+"_"+owner+"_"+payment+"_Context", {
        "royaltyPaymentInstruction": asset+"_"+owner+"_"+payment,

        "eventPeriod": newPeriod(),
        "location": chance.country(),
        "source": "Aresa Belgium (Pan�)",
        "channel": "SPOTIFY AB",
        "quantity": chance.natural({max: 1000}),
        "url": chance.url(),
        "extraMetrics": "",
        
        //"assetId": "18 (Clifford/Daunt/Hemmings/Howes/Stannard/Stride)",
        //"assetRight": "Streaming Perf."
    }];
};

function newPeriod() {
    return {
        "startDate": moment(chance.date()).toISOString(),
        "endDate": moment(chance.date()).toISOString()
    };
}