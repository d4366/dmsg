/* tslint:disable */

import { MessageStore } from '../src/messageStore';
import { ChaincodeMockStub, Transform } from '@theledger/fabric-mock-stub';
import * as chai from "chai";
import { expect } from "chai";

import * as fixture from './fixture';

chai.should();
chai.use(require('chai-things'));

const chaincode = new MessageStore();

let stubWithInit;


describe('Test MyChaincode', () => {
    
    before(() => {
        stubWithInit = new ChaincodeMockStub("MyMockStub", chaincode);
    });
    
    it("should init without issues", async () => {
        const stub = new ChaincodeMockStub("MyMockStub", chaincode);

        const response = await stub.mockInit("tx1", []);

        expect(response.status).to.eql(200)
    });

    it("should be able to publish message", async () => {
        let response, parsed;

        response = await stubWithInit.mockInvoke("txID2", [
            'publishMessage',
            'message',
            'sender'
        ]);

        response = await stubWithInit.mockInvoke("txID3", [
            'getMessages'
        ]);
        parsed = JSON.parse(response.payload.toString());
        console.log(parsed);

        expect(parsed).to.deep.equal([{sender: 'sender', message: 'message'}]);
    });
});
